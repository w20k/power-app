package com.w20k.powerdashboardapi.web.rest;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.w20k.powerdashboardapi.service.DashBoardService;
import com.w20k.powerdashboardapi.service.dto.PowerPlantsDto;
import com.w20k.powerdashboardapi.service.dto.WeatherStatsDto;
import com.w20k.powerdashboardapi.util.ResponseUtils;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(value = "/api/v1",
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE
)
@RequiredArgsConstructor
public class DashBoardResource {
    
    private final DashBoardService service;
    
    @GetMapping(value = "/power-stats")
    public ResponseEntity<PowerPlantsDto> getPower() {
        return ResponseUtils.mapOrNotFound(service.getFakePowerData());
    }
    
    @GetMapping(value = "/weather-stats")
    public ResponseEntity<WeatherStatsDto> getWeather() {
        return ResponseUtils.mapOrNotFound(service.getLatestWeatherDataOrUpdate());
    }
}
