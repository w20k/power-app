package com.w20k.powerdashboardapi.web.rest.errors;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ErrorConstants {
    
    public static final String ERR_CONCURRENCY_FAILURE = "error.concurrencyFailure";
    public static final String ERR_VALIDATION = "error.validation";
    public static final String FIELD_ERRORS_KEY = "fieldErrors";
    public static final String MESSAGE_KEY = "message";
    public static final String PATH_KEY = "path";
    public static final String VIOLATIONS_KEY = "violations";
}
