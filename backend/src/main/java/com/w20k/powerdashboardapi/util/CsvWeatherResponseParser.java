package com.w20k.powerdashboardapi.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;

@Component
public class CsvWeatherResponseParser {
    private static final CsvMapper mapper = new CsvMapper();
    
    public <T> List<T> read(Class<T> clazz, InputStream stream) throws IOException {
        return read(clazz, stream, ',');
    }
    
    public <T> List<T> read(Class<T> clazz, InputStream stream, char sep) throws IOException {
        ObjectReader reader = mapper
                .readerFor(clazz)
                .with(mapper
                        .schemaFor(clazz)
                        .withColumnSeparator(sep)
                        .withHeader()
                        .withColumnReordering(true)
                );
        
        return reader.<T>readValues(stream).readAll();
    }
}
