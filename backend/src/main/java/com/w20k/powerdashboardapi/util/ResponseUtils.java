package com.w20k.powerdashboardapi.util;

import java.util.Optional;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

public interface ResponseUtils {
    
    static <X> ResponseEntity<X> mapOrNotFound(Optional<X> maybeResponse) {
        return mapOrNotFound(maybeResponse, null);
    }
    
    static <X> ResponseEntity<X> mapOrNotFound(Optional<X> maybeResponse, HttpHeaders header) {
        return maybeResponse.map(response -> ResponseEntity.ok().headers(header).body(response))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }
}
