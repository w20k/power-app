package com.w20k.powerdashboardapi.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.RandomUtils;
import org.springframework.stereotype.Component;

import com.w20k.powerdashboardapi.service.dto.PowerPlantDto;
import com.w20k.powerdashboardapi.service.dto.PowerPlantsDto;

import lombok.EqualsAndHashCode;

@Component
@EqualsAndHashCode
public class PowerDataFakeGenerator {
    
    private static final List<String> POWER_PLANT_FAKE_NAMES = Arrays.asList("I9X", "B9X", "D0R", "Z9U", "S2Y", "W5H",
            "B8U", "X7N", "P0D", "Q7M", "P5J", "N9L", "E6L", "U6T", "K6F", "H8D", "Z7S", "K1C", "B1Q", "W3A", "D2Y",
            "C0O", "O7Y", "Q3L", "A9A", "X2X", "T3R", "A7S", "V5I", "G1M");
    
    public PowerPlantsDto generateFakePowerPlantsData() {
        List<PowerPlantDto> list = new ArrayList<>();
        BigDecimal total = BigDecimal.ZERO;
        for (String powerPlantFakeName : POWER_PLANT_FAKE_NAMES) {
            BigDecimal voltage = BigDecimal.valueOf(RandomUtils.nextFloat(180.00f, 300.00f))
                    .setScale(1, RoundingMode.CEILING);
            BigDecimal wattage = BigDecimal.valueOf(RandomUtils.nextFloat(50.00f, 750.00f))
                    .setScale(1, RoundingMode.CEILING);
            
            list.add(
                    PowerPlantDto.builder()
                            .id(powerPlantFakeName)
                            .wattage(wattage)
                            .voltage(voltage)
                            .build()
            );
            total = total.add(wattage);
        }
        total = total.setScale(2, RoundingMode.CEILING);
        
        return PowerPlantsDto.builder()
                .panels(list)
                .total(total)
                .lastUpdate(LocalDateTime
                        .now()
                        .toEpochSecond(ZoneOffset.UTC))
                .build();
    }
}
