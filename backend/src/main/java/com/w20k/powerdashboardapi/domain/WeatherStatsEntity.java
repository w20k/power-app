package com.w20k.powerdashboardapi.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@EqualsAndHashCode
@ToString
@Table(name = "weather_stats")
public class WeatherStatsEntity implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    
    @Column(name = "time", unique = true, updatable = false, nullable = false)
    private LocalDateTime time;
    
    @Column(name = "cloud_coverage")
    private BigDecimal cloudCoverage;
    
    @Column(name = "solar_activity")
    private BigDecimal solarActivity;
    
    @Column(name = "latitude")
    private Float latitude;
    
    @Column(name = "longitude")
    private Float longitude;
    
    @Column(name = "reftime")
    private String reftime;
    
    @CreationTimestamp
    @Column(name = "create_time", updatable = false)
    private Timestamp createTime;
    
}
