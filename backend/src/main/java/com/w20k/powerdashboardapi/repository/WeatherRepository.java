package com.w20k.powerdashboardapi.repository;


import java.time.LocalDateTime;
import java.util.Optional;

import javax.persistence.QueryHint;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;

import com.w20k.powerdashboardapi.domain.WeatherStatsEntity;

public interface WeatherRepository extends JpaRepository<WeatherStatsEntity, Long> {
    Optional<WeatherStatsEntity> findTopByOrderByIdDesc();
    
    @QueryHints(@QueryHint(name = org.hibernate.jpa.QueryHints.HINT_FETCH_SIZE, value = "1"))
    @Query(value = "SELECT (1=1) FROM WeatherStatsEntity WHERE time = :dateTime")
    Boolean existsIfDateTime(@Param("dateTime") LocalDateTime dateTime);
}
