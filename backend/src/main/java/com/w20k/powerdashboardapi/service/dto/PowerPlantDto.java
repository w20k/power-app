package com.w20k.powerdashboardapi.service.dto;

import java.math.BigDecimal;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PowerPlantDto {
    private String id;
    private BigDecimal voltage;
    private BigDecimal wattage;
}
