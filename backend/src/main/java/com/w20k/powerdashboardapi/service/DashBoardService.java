package com.w20k.powerdashboardapi.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.w20k.powerdashboardapi.domain.WeatherStatsEntity;
import com.w20k.powerdashboardapi.repository.WeatherRepository;
import com.w20k.powerdashboardapi.service.dto.PowerPlantsDto;
import com.w20k.powerdashboardapi.service.dto.WeatherAPIDto;
import com.w20k.powerdashboardapi.service.dto.WeatherStatsDto;
import com.w20k.powerdashboardapi.service.mapper.WeatherStatsMapper;
import com.w20k.powerdashboardapi.service.settings.WeatherSettings;
import com.w20k.powerdashboardapi.util.CsvWeatherResponseParser;
import com.w20k.powerdashboardapi.util.PowerDataFakeGenerator;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DashBoardService {
    
    private final WeatherSettings settings;
    private final RestTemplate restTemplate;
    private final CsvWeatherResponseParser csvUtilsReader;
    private final PowerDataFakeGenerator fakeData;
    private final WeatherRepository repo;
    private final WeatherStatsMapper mapper;
    
    public DashBoardService(WeatherSettings settings, RestTemplate restTemplate, CsvWeatherResponseParser csvUtilsReader, PowerDataFakeGenerator fakeData, WeatherRepository repo, WeatherStatsMapper mapper) {
        this.settings = settings;
        this.restTemplate = restTemplate;
        this.csvUtilsReader = csvUtilsReader;
        this.fakeData = fakeData;
        this.repo = repo;
        this.mapper = mapper;
    }
    
    public Optional<PowerPlantsDto> getFakePowerData() {
        return Optional.of(fakeData.generateFakePowerPlantsData());
    }
    
    public Optional<WeatherStatsDto> getLatestWeatherDataOrUpdate() {
        LocalDateTime dateTime = null;
        String error = null;
        List<WeatherStatsEntity> savedData = new ArrayList<>();
        
        try {
            Optional<WeatherStatsEntity> latestUpdate = repo.findTopByOrderByIdDesc();
            if (latestUpdate.isPresent()) {
                LocalDateTime lastUpdatePlus15mins = latestUpdate
                        .get()
                        .getCreateTime()
                        .toLocalDateTime()
                        .plusMinutes(15);
                if (LocalDateTime.now().compareTo(lastUpdatePlus15mins) > -1) {
                    savedData = repo.findAll();
                    dateTime = LocalDateTime.now();
                }
            } else {
                ResponseEntity<String> weatherStatsResponse = restTemplate.getForEntity(settings.getWeatherStatsUrl(), String.class);
                List<WeatherAPIDto> apiData = getWeatherApiDto(weatherStatsResponse);
                if (apiData != null && !apiData.isEmpty()) {
                    List<WeatherStatsEntity> weatherData = mapper.weatherApiToWeatherStatsEntities(apiData);
                    List<WeatherStatsEntity> dataToInsert = new ArrayList<>();
                    
                    long count = repo.count();
                    if (count > 0) {
                        for (WeatherStatsEntity entity : weatherData) {
                            if (!repo.existsIfDateTime(entity.getTime())) {
                                dataToInsert.add(entity);
                            }
                        }
                    } else {
                        dataToInsert = weatherData;
                    }
                    
                    if (!dataToInsert.isEmpty() || count == 0) {
                        savedData = repo.saveAll(dataToInsert);
                        dateTime = LocalDateTime.now();
                    }
                } else {
                    error = "Couldn't save/proceed the latest forecasting data;";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            error = "Couldn't get the latest forecasting data;";
        }
        
        Long dateTimeLong = dateTime != null ? dateTime.toEpochSecond(ZoneOffset.UTC) : null;
        
        return Optional.ofNullable(WeatherStatsDto.builder()
                .data(mapper.weatherStatsToDTO(savedData))
                .lastUpdate(dateTimeLong)
                .error(error)
                .build());
    }
    
    private List<WeatherAPIDto> getWeatherApiDto(ResponseEntity<String> responseEntity) throws IOException {
        if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
            if (responseEntity.getBody() != null && !responseEntity.getBody().isEmpty()) {
                byte[] bodyBytes = responseEntity
                        .getBody()
                        .getBytes(StandardCharsets.UTF_8);
                InputStream stream = new ByteArrayInputStream(bodyBytes);
                return csvUtilsReader.read(WeatherAPIDto.class, stream);
            }
        }
        return null;
    }
    
}
