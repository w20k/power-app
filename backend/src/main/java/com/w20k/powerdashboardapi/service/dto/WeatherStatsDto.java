package com.w20k.powerdashboardapi.service.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class WeatherStatsDto implements Serializable {
    private List<WeatherStatDto> data;
    private Long lastUpdate;
    private String error;
}
