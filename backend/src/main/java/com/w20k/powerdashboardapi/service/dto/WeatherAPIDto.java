package com.w20k.powerdashboardapi.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherAPIDto implements Serializable {
    
    @JsonProperty("axis:latitude")
    private Float latitude;
    
    @JsonProperty("axis:longitude")
    private Float longitude;
    
    @JsonProperty("axis:reftime")
    private String reftime;
    
    @JsonProperty("axis:time")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime time;
    
    @JsonProperty("data:av_ttl_cld")
    private BigDecimal cloudCoverage;
    
    @JsonProperty("data:av_swsfcdown")
    private BigDecimal solarActivity;
}
