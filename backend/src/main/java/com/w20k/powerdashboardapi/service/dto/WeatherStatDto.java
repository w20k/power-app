package com.w20k.powerdashboardapi.service.dto;


import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
public class WeatherStatDto implements Serializable {
    private LocalDateTime time;
    private BigDecimal cloudCoverage;
    private BigDecimal solarActivity;
}
