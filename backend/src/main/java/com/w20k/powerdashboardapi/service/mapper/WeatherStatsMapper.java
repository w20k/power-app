package com.w20k.powerdashboardapi.service.mapper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.w20k.powerdashboardapi.domain.WeatherStatsEntity;
import com.w20k.powerdashboardapi.service.dto.WeatherAPIDto;
import com.w20k.powerdashboardapi.service.dto.WeatherStatDto;

@Service
public class WeatherStatsMapper {
    
    public List<WeatherStatsEntity> weatherApiToWeatherStatsEntities(List<WeatherAPIDto> apiData) {
        return apiData.stream()
                .filter(Objects::nonNull)
                .map(this::weatherStatsEntity)
                .collect(Collectors.toList());
    }
    
    private WeatherStatsEntity weatherStatsEntity(WeatherAPIDto apiDto) {
        if (apiDto == null) {
            return null;
        } else {
            WeatherStatsEntity entity = new WeatherStatsEntity();
            entity.setLatitude(apiDto.getLatitude());
            entity.setLongitude(apiDto.getLongitude());
            entity.setReftime(apiDto.getReftime());
            entity.setTime(apiDto.getTime());
            entity.setCloudCoverage(apiDto.getCloudCoverage());
            entity.setSolarActivity(apiDto.getSolarActivity());
            return entity;
        }
    }
    
    public List<WeatherStatDto> weatherStatsToDTO(List<WeatherStatsEntity> stats) {
        return stats.stream()
                .filter(Objects::nonNull)
                .map(this::weatherStatToDTO)
                .collect(Collectors.toList());
    }
    
    public WeatherStatDto weatherStatToDTO(WeatherStatsEntity stat) {
        return WeatherStatDto.builder()
                .time(stat.getTime())
                .cloudCoverage(stat.getCloudCoverage())
                .solarActivity(stat.getSolarActivity())
                .build();
    }
    
}
