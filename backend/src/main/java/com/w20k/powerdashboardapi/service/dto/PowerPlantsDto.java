package com.w20k.powerdashboardapi.service.dto;

import java.math.BigDecimal;
import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PowerPlantsDto {
    private List<PowerPlantDto> panels;
    private BigDecimal total;
    private Long lastUpdate;
    private String error;
}
