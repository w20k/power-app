package com.w20k.powerdashboardapi.service.settings;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Validated
@Component
@ConfigurationProperties(prefix = "weather-api")
public class WeatherSettings {
    
    @NotNull
    @NotEmpty
    private String apiKey;
    
    @NotEmpty
    private String baseUrl;
    
    @NotEmpty
    private String weatherStatsUrl;
}
