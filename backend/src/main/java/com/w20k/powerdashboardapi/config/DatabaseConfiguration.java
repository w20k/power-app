package com.w20k.powerdashboardapi.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
@EnableJpaRepositories("com.w20k.powerdashboardapi.repository")
public class DatabaseConfiguration {
}
