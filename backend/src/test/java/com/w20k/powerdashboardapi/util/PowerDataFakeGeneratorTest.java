package com.w20k.powerdashboardapi.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.w20k.powerdashboardapi.service.dto.PowerPlantDto;
import com.w20k.powerdashboardapi.service.dto.PowerPlantsDto;

class PowerDataFakeGeneratorTest {
    
    private static final List<String> idsArray = Arrays.asList("I9X", "B9X", "D0R", "Z9U", "S2Y", "W5H",
            "B8U", "X7N", "P0D", "Q7M", "P5J", "N9L", "E6L", "U6T", "K6F", "H8D", "Z7S", "K1C", "B1Q", "W3A", "D2Y",
            "C0O", "O7Y", "Q3L", "A9A", "X2X", "T3R", "A7S", "V5I", "G1M");
    
    private PowerDataFakeGenerator victim;
    
    @BeforeEach
    void setup() {
        victim = new PowerDataFakeGenerator();
    }
    
    @Test
    @DisplayName("Test returns 30 panels Ids and non-null values")
    void shouldHave30PanelsWithIdsAndNotNullValues() {
        // Execute the service call
        PowerPlantsDto powerPlants = victim.generateFakePowerPlantsData();
        
        // Assert the response
        assertThat(powerPlants.getPanels(), hasSize(30));
        assertThat(powerPlants.getError(), is(nullValue()));
        assertThat(powerPlants.getLastUpdate(), is(greaterThan(0L)));
        assertThat(powerPlants.getTotal(), is(greaterThan(BigDecimal.ZERO)));
        assertArrayEquals(powerPlants.getPanels().stream().map(PowerPlantDto::getId).toArray(), idsArray.toArray());
    }
}
