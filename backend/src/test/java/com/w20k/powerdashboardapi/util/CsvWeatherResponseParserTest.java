package com.w20k.powerdashboardapi.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.w20k.powerdashboardapi.service.dto.WeatherAPIDto;

import lombok.Data;

class CsvWeatherResponseParserTest {
    
    private CsvWeatherResponseParser csvUtils;
    
    @BeforeEach
    public void init() {
        csvUtils = new CsvWeatherResponseParser();
    }
    
    @Test
    void shouldParseValidadlyWeatherData() throws IOException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("test_data/av_swsfcdown.csv");
        List<WeatherAPIDto> result = csvUtils.read(WeatherAPIDto.class, inputStream);
        
        assertThat(result, hasSize(50));
        assertThat(result.get(0), is(not(nullValue())));
        assertThat(result.get(0).getReftime(), equalTo("2020-06-27T00:00:00"));
        assertThat(result.get(0).getLatitude(), is(49.453125F));
        assertThat(result.get(0).getLongitude(), is(-50.625F));
        assertThat(result.get(0).getSolarActivity(), is(BigDecimal.valueOf(61.780967712402344)));
    }
    
    @Disabled
    @Test
    void shouldParseCSVfromWeatherData() throws IOException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("test_data/av_swsfcdown.csv");
        List<WeatherAPIDto> result = csvUtils.read(WeatherAPIDto.class, inputStream);
        
        InputStream inputStream2 = getClass().getClassLoader().getResourceAsStream("test_data/av_ttl_cld.csv");
        List<WeatherAPIDto> result2 = csvUtils.read(WeatherAPIDto.class, inputStream2);
        
        int compare = result.get(0).getTime().compareTo(result2.get(0).getTime());
        if (compare < 0) {
            result.remove(0);
        } else if (compare > 0) {
            result2.remove(0);
        }
        int compare2 = result.get(result.size() - 1).getTime().compareTo(result2.get(result2.size() - 1).getTime());
        if (compare2 > 0) {
            result.remove(result.size() - 1);
        } else if (compare2 < 0) {
            result2.remove(result2.size() - 1);
        }
        
        Map<LocalDateTime, WeatherAPIDto> map = new HashMap<>();
        for (WeatherAPIDto dtoResult : result) {
            map.put(dtoResult.getTime(), dtoResult);
        }
        for (WeatherAPIDto dtoResult2 : result2) {
            LocalDateTime key = dtoResult2.getTime();
            if (map.containsKey(key)) {
                map.get(key).setCloudCoverage(dtoResult2.getCloudCoverage());
            }
        }
        
        List<WeatherAPIDto> apiData = new ArrayList<>(map.values());
        
        System.out.println("End");
    }
    
    @Disabled
    @Test
    void shouldParseCSVfromPowerData() throws IOException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("test_data/power_sample.csv");
        List<PowerPlantTest> result = csvUtils.read(PowerPlantTest.class, inputStream, '\t');
        
        BigDecimal energy = Collections
                .max(result, Comparator.comparing(PowerPlantTest::getEnergy)).getEnergy();
        
        BigDecimal efficiency = Collections
                .max(result, Comparator.comparing(PowerPlantTest::getEfficiency)).getEfficiency();
        
        BigDecimal power = Collections
                .max(result, Comparator.comparing(PowerPlantTest::getPower)).getPower();
        
        BigDecimal average = Collections
                .max(result, Comparator.comparing(PowerPlantTest::getAverage)).getAverage();
        
        BigDecimal normalised = Collections
                .max(result, Comparator.comparing(PowerPlantTest::getNormalised)).getNormalised();
        
        BigDecimal temperature = Collections
                .max(result, Comparator.comparing(PowerPlantTest::getTemperature)).getTemperature();
        
        BigDecimal voltage = Collections
                .max(result, Comparator.comparing(PowerPlantTest::getVoltage)).getVoltage();
        
        BigDecimal energyUsed = Collections
                .max(result, Comparator.comparing(PowerPlantTest::getEnergyUsed)).getEnergyUsed();
        
        ///////////////
        
        BigDecimal energyMin = Collections
                .min(result, Comparator.comparing(PowerPlantTest::getEnergy)).getEnergy();
        
        BigDecimal efficiencyMin = Collections
                .min(result, Comparator.comparing(PowerPlantTest::getEfficiency)).getEfficiency();
        
        BigDecimal powerMin = Collections
                .min(result, Comparator.comparing(PowerPlantTest::getPower)).getPower();
        
        BigDecimal averageMin = Collections
                .min(result, Comparator.comparing(PowerPlantTest::getAverage)).getAverage();
        
        BigDecimal normalisedMin = Collections
                .min(result, Comparator.comparing(PowerPlantTest::getNormalised)).getNormalised();
        
        BigDecimal temperatureMin = Collections
                .min(result, Comparator.comparing(PowerPlantTest::getTemperature)).getTemperature();
        
        BigDecimal voltageMin = Collections
                .min(result, Comparator.comparing(PowerPlantTest::getVoltage)).getVoltage();
        
        BigDecimal energyUsedMin = Collections
                .min(result, Comparator.comparing(PowerPlantTest::getEnergyUsed)).getEnergyUsed();
        
        System.out.println("End");
    }
    
    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class PowerPlantTest {
        
        @JsonProperty("Date")
        private String date;
        
        @JsonProperty("Time")
        private String time;
        
        @JsonProperty("Energy")
        private BigDecimal energy;
        
        @JsonProperty("Efficiency")
        private BigDecimal efficiency;
        
        @JsonProperty("Power")
        private BigDecimal power;
        
        @JsonProperty("Average")
        private BigDecimal average;
        
        @JsonProperty("Normalised")
        private BigDecimal normalised;
        
        @JsonProperty("Temperature")
        private BigDecimal temperature;
        
        @JsonProperty("Voltage")
        private BigDecimal voltage;
        
        @JsonProperty("EnergyUsed")
        private BigDecimal energyUsed;
    }
}
