package com.w20k.powerdashboardapi.web.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import com.w20k.powerdashboardapi.PowerDashboardapiApplication;

@AutoConfigureMockMvc
@SpringBootTest(classes = {PowerDashboardapiApplication.class})
class DashBoardResourceIT {
    
    /*
    * Simple IT test, not finished (WIP part)
    * */
    
    @Autowired
    private MockMvc restUserMockMvc;
    
    @Test
    @Transactional
    public void getPower() throws Exception {
        restUserMockMvc.perform(get("/api/v1/power-stats")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").isMap());
    }
    
    
    @Test
    @Transactional
    public void getExceptionForPower() throws Exception {
        restUserMockMvc.perform(get("/api/v1/power-stats")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$").isMap())
        ;
    }
}
