package com.w20k.powerdashboardapi.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.web.client.RestTemplate;

import com.w20k.powerdashboardapi.domain.WeatherStatsEntity;
import com.w20k.powerdashboardapi.repository.WeatherRepository;
import com.w20k.powerdashboardapi.service.settings.WeatherSettings;
import com.w20k.powerdashboardapi.util.CsvWeatherResponseParser;

@Disabled
class DashBoardServiceTest {
    
    @Mock
    private WeatherSettings settings;
    @Mock
    private RestTemplate restTemplate;
    @Mock
    private CsvWeatherResponseParser csvUtilsReader;
    @Mock
    private WeatherRepository repo;
    
    @InjectMocks
    private DashBoardService victim;
    
    private LocalDateTime now;
    
    
    @BeforeAll
    void init() {
        now = LocalDateTime.now();
    }
    
    @Test
    void shouldReturnCachedResponseIf15minsHaventPassed() {
    
    }
    
    @Test
    void shouldReturnRealResultAndInsertOnlyNew() {
        when(repo.findTopByOrderByIdDesc())
                .thenReturn(getOptionalStat());
        when(repo.existsIfDateTime(any())).thenReturn(false);
        when(repo.saveAll(anyList())).thenAnswer(i -> i.getArguments()[0]);
        
        when(repo.count()).thenReturn(1L);
    }
    
    @Test
    void shouldReturnRealResultAndInsert() {
        when(repo.existsIfDateTime(any())).thenReturn(true);
        when(repo.count()).thenReturn(1L);
    }
    
    
    private Optional<WeatherStatsEntity> getOptionalStat() {
        WeatherStatsEntity weatherStatsEntity = new WeatherStatsEntity();
        weatherStatsEntity.setLatitude(50F);
        weatherStatsEntity.setLongitude(50F);
        weatherStatsEntity.setReftime("2020-06-27T00:00:00");
        weatherStatsEntity.setTime(now);
        weatherStatsEntity.setCloudCoverage(BigDecimal.valueOf(61.780967712402344));
        weatherStatsEntity.setSolarActivity(BigDecimal.valueOf(1.0));
        return Optional.of(weatherStatsEntity);
    }
}
