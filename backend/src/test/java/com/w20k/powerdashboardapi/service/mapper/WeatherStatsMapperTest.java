package com.w20k.powerdashboardapi.service.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.w20k.powerdashboardapi.domain.WeatherStatsEntity;
import com.w20k.powerdashboardapi.service.dto.WeatherAPIDto;
import com.w20k.powerdashboardapi.service.dto.WeatherStatDto;

class WeatherStatsMapperTest {
    
    private WeatherStatsMapper mapper;
    private WeatherAPIDto weatherAPIDto;
    private WeatherStatsEntity weatherStatsEntity;
    private LocalDateTime now;
    
    @BeforeEach
    public void init() {
        now = LocalDateTime.now();
        mapper = new WeatherStatsMapper();
        
        weatherAPIDto = new WeatherAPIDto();
        weatherAPIDto.setLatitude(50F);
        weatherAPIDto.setLongitude(50F);
        weatherAPIDto.setReftime("2020-06-27T00:00:00");
        weatherAPIDto.setTime(now);
        weatherAPIDto.setCloudCoverage(BigDecimal.valueOf(61.780967712402344));
        weatherAPIDto.setSolarActivity(BigDecimal.valueOf(1.0));
        
        weatherStatsEntity = new WeatherStatsEntity();
        weatherStatsEntity.setLatitude(50F);
        weatherStatsEntity.setLongitude(50F);
        weatherStatsEntity.setReftime("2020-06-27T00:00:00");
        weatherStatsEntity.setTime(now);
        weatherStatsEntity.setCloudCoverage(BigDecimal.valueOf(61.780967712402344));
        weatherStatsEntity.setSolarActivity(BigDecimal.valueOf(1.0));
    }
    
    
    @Test
    public void shouldMapWeatherApiToWeatherStatsEntities() {
        List<WeatherAPIDto> apis = new ArrayList<>();
        apis.add(null);
        apis.add(weatherAPIDto);
        
        List<WeatherStatsEntity> statsEntities = mapper.weatherApiToWeatherStatsEntities(apis);
        
        assertThat(statsEntities).isNotEmpty();
        assertThat(statsEntities).size().isEqualTo(1);
        assertThat(statsEntities.get(0).getSolarActivity()).isEqualTo(BigDecimal.valueOf(1.0));
    }
    
    @Test
    public void shouldBeEmptyOnNullApis() {
        List<WeatherAPIDto> apis = new ArrayList<>();
        apis.add(null);
        
        List<WeatherStatsEntity> statsEntities = mapper.weatherApiToWeatherStatsEntities(apis);
        
        assertThat(statsEntities).isEmpty();
        assertThat(statsEntities).size().isEqualTo(0);
    }
    
    @Test
    public void shouldWeatherStatsToDTO() {
        List<WeatherStatsEntity> entities = new ArrayList<>();
        entities.add(null);
        entities.add(weatherStatsEntity);
        
        
        List<WeatherStatDto> weatherStatDtos = mapper.weatherStatsToDTO(entities);
        
        assertThat(weatherStatDtos).isNotEmpty();
        assertThat(weatherStatDtos).size().isEqualTo(1);
        assertThat(weatherStatDtos.get(0).getSolarActivity()).isEqualTo(BigDecimal.valueOf(1.0));
    }
    
    @Test
    public void shouldBeNullIfWeatherStatsNull() {
        List<WeatherStatsEntity> entities = new ArrayList<>();
        entities.add(null);
        
        List<WeatherStatDto> weatherStatDtos = mapper.weatherStatsToDTO(entities);
        
        assertThat(weatherStatDtos).isEmpty();
        assertThat(weatherStatDtos).size().isEqualTo(0);
    }
}
