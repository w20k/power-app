import { createSlice, PayloadAction } from '@reduxjs/toolkit'

import { getWeatherStats, IWeatherStatsDisplayState } from '../../api/microserviceAPI'
import { AppThunk } from '../../app/store'

let initialState: IWeatherStatsDisplayState = {
  data: [],
  lastUpdate: '',
  error: null
}

const weatherStatsDisplaySlice = createSlice({
  name: 'weatherStatsDisplay',
  initialState,
  reducers: {
    getWeatherStatsSuccess(state, action: PayloadAction<IWeatherStatsDisplayState>) {
      state.data = action.payload.data
      state.lastUpdate = action.payload.lastUpdate
      state.error = null
    },
    getWeatherStatsFailed(state, action: PayloadAction<string>) {
      state.error = action.payload
    }
  }
})

export const {
  getWeatherStatsSuccess,
  getWeatherStatsFailed
} = weatherStatsDisplaySlice.actions

export default weatherStatsDisplaySlice.reducer

export const fetchLatestWeatherData = () : AppThunk => async dispatch => {
  try {
    const weatherStats = await getWeatherStats()
    dispatch(getWeatherStatsSuccess(weatherStats))
  } catch (err) {
    dispatch(getWeatherStatsFailed(err.toString()))
  }
}
