import { createSlice, PayloadAction } from '@reduxjs/toolkit'

import { getPanelsStats, IPanelListData } from '../../api/microserviceAPI'
import { AppThunk } from '../../app/store'

let initialState: IPanelListData = {
  panels: [],
  total: 0,
  lastUpdate: '',
  error: null
}

const panelListDisplaySlice = createSlice({
  name: 'panelListDisplay',
  initialState,
  reducers: {
    getPanelsStatsSuccess(state, action: PayloadAction<IPanelListData>) {
      state.panels = action.payload.panels
      state.total = action.payload.total
      state.lastUpdate = action.payload.lastUpdate
      state.error = null
    },
    getPanelsStatsFailed(state, action: PayloadAction<string>) {
      state.error = action.payload
    }
  }
})

export const {
  getPanelsStatsSuccess,
  getPanelsStatsFailed
} = panelListDisplaySlice.actions

export default panelListDisplaySlice.reducer

export const fetchLatestPanelsData = (): AppThunk => async dispatch => {
  try {
    const panelsStats = await getPanelsStats()
    dispatch(getPanelsStatsSuccess(panelsStats))
  } catch (err) {
    dispatch(getPanelsStatsFailed(err.toString()))
  }
}
