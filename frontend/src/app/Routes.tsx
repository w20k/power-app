import * as React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { DashBoard } from '../components/DashBoard'

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route path="/" component={DashBoard}/>
    </Switch>
  </BrowserRouter>
)

export default Routes
