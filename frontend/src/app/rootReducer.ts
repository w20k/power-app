import { combineReducers } from '@reduxjs/toolkit'

import weatherStatsDisplayReducer from 'features/weatherStatsDisplay/weatherStatsDisplaySlice'
import panelListDisplayReducer from 'features/panelsList/panelListDisplaySlice'

const rootReducer = combineReducers({
  weatherStatsDisplay: weatherStatsDisplayReducer,
  panelListDisplay: panelListDisplayReducer
})

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer
