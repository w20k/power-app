import React from 'react'

import Routes from './Routes'

const App: React.FC = () => {
  let content = (
    <React.Fragment>
      <Routes/>
    </React.Fragment>
  )

  return <div className="App">{content}</div>
}

export default App
