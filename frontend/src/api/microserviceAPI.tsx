import axios from 'axios'

const API_URL = process.env.MICROSERVICE_API_BASE_URL || 'http://localhost:8081/api/v1'

export const panelsStatsDelay = 10 * 1000
export const weatherDelay = 15 * 60 * 1000

export interface IPanel {
  id: string;
  wattage: number;
  voltage: number;
}

export interface IPanelListData {
  panels: IPanel[],
  total: number,
  lastUpdate: string,
  error: string | null
}

export interface IWeatherStatsData {
  time: string;
  solar_activity: number;
  cloud_coverage: number;
}

export interface IWeatherStatsDisplayState {
  data: IWeatherStatsData[]
  lastUpdate: string,
  error: string | null
}

export async function getWeatherStats() {
  const options = {
    data: {},
    headers: { 'Content-Type': 'application/json' }
  }
  const url = `${API_URL}/weather-stats`

  const { data } = await axios.get<IWeatherStatsDisplayState>(url, options)
  return data
}

export async function getPanelsStats() {
  const options = {
    data: {},
    headers: { 'Content-Type': 'application/json' }
  }
  const url = `${API_URL}/power-stats`

  const { data } = await axios.get<IPanelListData>(url, options)
  return data
}
