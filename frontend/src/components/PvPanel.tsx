import React from 'react'

import classNames from 'classnames'
import { createStyles, Paper, Theme, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { IPanel } from '../api/microserviceAPI'


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    panel: {
      height: 70,
      padding: '8px',
      cursor: 'default',
      display: 'flex',
      justifyContent: 'space-between',
      flexDirection: 'column',
      transition: 'all .2s ease-in-out'
    },
    panelOutput: {
      alignItems: 'stretch',
      display: 'flex',
      justifyContent: 'space-around'
    },
    voltage: {
      padding: 2,
      fontSize: '1.5em'
    },
    wattage: {
      padding: 2,
      fontSize: '1.5em'
    },

    postfix: {
      paddingLeft: 2.5,
      fontSize: '0.5em',
      fontWeight: 400,
    },

    title: {
      fontSize: '1em',
      fontWeight: 600,
      textAlign: 'left',
    }
  })
)

interface IProps {
  className?: string;
  panel: IPanel;
}

export const PvPanel =
  ({
     className,
     panel,
     ...rest
   }: IProps) => {
    const classes = useStyles()
    return (
      <Paper
        className={classNames(
          classes.panel,
          className
        )}
        elevation={1}
        {...rest}
      >
        <Typography className={classes.title}>Id: {panel.id}</Typography>
        <div className={classes.panelOutput}>
          <Typography className={classes.voltage}>
            {panel.voltage}<span className={classes.postfix}>V</span>
          </Typography>
          <Typography className={classes.wattage}>
            {panel.wattage}<span className={classes.postfix}>W</span>
          </Typography>
        </div>
      </Paper>
    )
  }
