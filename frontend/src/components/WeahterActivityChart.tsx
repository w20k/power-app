import * as React from 'react';

import { Paper } from '@material-ui/core'
import moment from 'moment'
import { Line, LineChart, ResponsiveContainer, Tooltip, XAxis } from 'recharts'
import { IWeatherStatsData } from '../api/microserviceAPI'

interface IProps {
  chartTitle: string
  lineKey: string
  classes: {
    panel: string,
    label: string,
  }
  weatherData: IWeatherStatsData[],
}


export const WeatherActivityChart = ({ chartTitle, lineKey, classes, weatherData }: IProps) => {
  const format = 'HH:mm'
  const unit = 'hours'

  const now = moment(),
    plus24Hours = moment().add(24, unit)

  const data = weatherData
  .filter(item => item && moment(item.time).isBetween(now, plus24Hours))
  .map((item: IWeatherStatsData) => ({
    name: moment(item.time).format(format),
    kW: Math.floor(item.solar_activity),
    Coverage: Math.floor(item.cloud_coverage * 100),
  }))

  return (
    <Paper className={classes.panel} elevation={1}>
      <div className={classes.label}>{chartTitle}</div>
      <ResponsiveContainer>
        <LineChart height={80} data={data}>
          <Tooltip/>
          <XAxis dataKey="name"/>
          <Line type="monotone" dataKey={lineKey} stroke="#ff7300"/>
        </LineChart>
      </ResponsiveContainer>
    </Paper>
  )
}
