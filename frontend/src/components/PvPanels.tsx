import React from 'react'

import classNames from 'classnames'
import { createStyles, Grid, Theme } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

import { PvPanel } from './PvPanel'
import { IPanel } from '../api/microserviceAPI'

// import { Label } from 'api/githubAPI'
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    panels: {
      flexGrow: 1
    },
    link: {
      textDecoration: 'none',
      height: 320,
      cursor: 'default',
      outline: 'none'
    }
  })
)

interface IProps {
  className?: string;
  panels: IPanel[];
}

export const PvPanels = ({ className, panels }: IProps) => {
  const classes = useStyles()

  return (
    <div className={className}>
      <div className={classNames(className, classes.panels)}>
        <Grid container spacing={4}>
          {
            panels.map((panel, i) => (
              <Grid key={panel.id} item xs={12} sm={6} md={3} lg={2} >
                <PvPanel panel={panel}/>
              </Grid>
            ))
          }
        </Grid>
      </div>
    </div>
  )
}
