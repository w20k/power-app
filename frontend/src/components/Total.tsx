import React from 'react';

import { createStyles, Paper, Theme, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    panel: {
      height: 90,
      padding: '8px',
      cursor: 'default',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      flexDirection: 'column',
      transition: 'all .2s ease-in-out',
    },
    totalClass: {
      fontSize: '2em',
    },
    postfix: {
      fontSize: '0.5em',
      paddingLeft: 5,
    }
  })
)


interface IProps {
  total: number;
}

export const Total = ({ total }: IProps) => {
  const classes = useStyles()

  return (
    <Paper className={classes.panel} elevation={1}>
      <Typography variant="caption" display="block" gutterBottom>
        Total output
      </Typography>
      <Typography className={classes.totalClass} variant={'h3'}>
        {total}<span className={classes.postfix}>kW</span>
      </Typography>
    </Paper>
  )
}
