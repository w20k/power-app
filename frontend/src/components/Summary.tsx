import React from 'react'

import { createStyles, Grid, Theme } from '@material-ui/core'

import { WeatherActivityChart } from 'components/WeahterActivityChart'
import { Total } from './Total'
import { makeStyles } from '@material-ui/core/styles'
import { IWeatherStatsData } from '../api/microserviceAPI'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    panel: {
      height: 90,
      padding: '0px !important',
      cursor: 'default',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      flexDirection: 'column',
      transition: 'all .2s ease-in-out'
    },
    label: {
      color: theme.palette.text.primary
    }
  }))

interface IProps {
  className?: string;
  total: number;
  weatherData: IWeatherStatsData[],
}

export const Summary = ({ className, total, weatherData }: IProps) => {
  const chartClasses = useStyles()

  return (
    <div className={className}>
      <Grid container spacing={4}>
        <Grid item xs={12} sm={4} md={4} lg={2}>
          <Total total={total}/>
        </Grid>
        <Grid item xs={12} sm={4} md={4} lg={5}>
          <WeatherActivityChart
            chartTitle={'Solar activity'}
            lineKey={'kW'}
            classes={chartClasses}
            weatherData={weatherData}
          />
        </Grid>
        <Grid item xs={12} sm={4} md={4} lg={5}>
          <WeatherActivityChart
            chartTitle={'Cloud coverage'}
            lineKey={'Coverage'}
            classes={chartClasses}
            weatherData={weatherData}
          />
        </Grid>
      </Grid>
    </div>
  )
}
