import React, { useState } from 'react'

import { AppBar, Button, createStyles, Theme, Toolbar, Typography } from '@material-ui/core'
import { useDispatch, useSelector } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'

import { RootState } from '../app/rootReducer'
import { Summary } from './Summary'
import { PvPanels } from './PvPanels'

import { fetchLatestPanelsData } from '../features/panelsList/panelListDisplaySlice'
import { fetchLatestWeatherData } from '../features/weatherStatsDisplay/weatherStatsDisplaySlice'

import useInterval from './../utils/intervalEffect'
import { panelsStatsDelay, weatherDelay } from '../api/microserviceAPI'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      minHeight: '100vh'
    },
    toolbar: {
      minHeight: 40
    },
    title: {
      fontWeight: 300,
      flex: 1
    },
    grid: {
      padding: 10
    }
  })
)


export const DashBoard = () => {
  const classes = useStyles()
  const dispatch = useDispatch()
  let [count, setCount] = useState(0)

  const { panels, total } = useSelector((state: RootState) => state.panelListDisplay)
  const { data } = useSelector((state: RootState) => state.weatherStatsDisplay)

  useInterval(() => {
    dispatch(fetchLatestPanelsData())
    setCount(count + 1)
  }, panelsStatsDelay, true)

  useInterval(() => {
    dispatch(fetchLatestWeatherData())
    setCount(count + 1)
  }, weatherDelay, true)

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar className={classes.toolbar}>
          <Typography color="inherit" className={classes.title}>
            Solar Panel Dashboard
          </Typography>
          <Button color="inherit">Login</Button>
        </Toolbar>
      </AppBar>
      <Summary
        className={classes.grid}
        total={total}
        weatherData={data}
      />
      <PvPanels
        className={classes.grid}
        panels={panels}
      />
    </div>
  )
}
